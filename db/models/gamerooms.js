'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class gameRooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
         // define association here
         gameRooms.belongsTo(models.UserGames,{foreignKey: "idPlayer1" , as: "player1"})
         gameRooms.belongsTo(models.UserGames,{foreignKey: "idPlayer2", as : "player2"})
    }
  }
  gameRooms.init({
    roomName: DataTypes.STRING,
    idPlayer1: DataTypes.INTEGER,
    player1Choice: DataTypes.STRING,
    idPlayer2: DataTypes.INTEGER,
    player2Choice: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'gameRooms',
  });
  return gameRooms;
};