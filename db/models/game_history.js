'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      game_history.belongsTo(models.UserGames,{foreignKey: "user_id"})
    }
  }
  game_history.init({
    opponent: DataTypes.STRING,
    result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'game_history',
  });
  return game_history;
};