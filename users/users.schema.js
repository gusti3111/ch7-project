const registrationSchema = {
  username: {
    isString: {
      errorMessage: "username invalid format",
    },
    notEmpty: {
      errorMessage: "username must not be empty",
    },
  },
  email: {
    isString: {
      errorMessage: "email invalid format",
    },
    isEmail: {
      errorMessage: "email formate not valid",
    },
    notEmpty: {
      errorMessage: "email must not empty",
    },
  },
  password: {
    isString: {
      errorMessage: "password invalid format",
    },
    isLength: {
      options: { min: 8 },
      errorMessage: "Password at least 8 character",
    },
    notEmpty: {
      errorMessage: "password must not empty",
    },
  },
};
const loginSchema = {
  username: {
    isString: true,
    notEmpty: {
      errorMessage: "Username must not be empty",
    },

    optional: { options: { nullable: true } },
  },

  password: {
    isString: true,
    notEmpty: {
      errorMessage: "Password must not be empty",
    },

    optional: { options: { nullable: true } },
  },
};

// update schema for users
const updateSchema = {
  // check request body
  firstname: {
    isString: {
      errorMessage: "firstname invalid format",
    },
    notEmpty: {
      errorMessage: "first must not be empty",
    },
  },
  lastname: {
    isString: {
      errorMessage: "lastname invalid format",
    },
    notEmpty: {
      errorMessage: "lastname must not be empty",
    },
  },
  fullname: {
    isString: {
      errorMessage: "fullname is invalid format",
    },
    notEmpty: {
      errorMessage: "fullname must not be empty",
    },
  },
  address: {
    isString: {
      errorMessage: "your email is invalid format",
    },
    notEmpty: {
      errorMessage: "address must not be empty",
    },
  },
  user_id: {
    in: ["params"],
    isInt: {
      errorMessage: "user_id invalid format",
    },
    notEmpty: {
      errorMessage: "user_id must not be empty",
    },
  },
};

// get by id schema users
const getByUserIdSchema = {
  user_id: {
    in: ["params"],
    isInt: {
      errorMessage: "user_id invalid format",
    },
    notEmpty: {
      errorMessage: "user_id must not be empty",
    },
  },
};

module.exports = {
  registrationSchema,
  loginSchema,
  updateSchema,
  getByUserIdSchema,
};
