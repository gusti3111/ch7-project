const db = require("../db/models/");
const md5 = require("md5");



// create class for wrapping up my models
class usersModel {
  pushUserGames = async (dataReq) => {
    try {
      const pushuser = db.UserGames.create({
        username: dataReq.username,
        email: dataReq.email,
        password: md5(dataReq.password)
      });

      return pushuser;



    } catch (error) {
      console.log(error)
    }




    // // record new data to table userGames
  };
  // get all data from database
  getAllUsers = async () => {
    try {
    
      const getAll = await db.UserGames.findAll()

      return getAll;


    } catch (error) {
      console.log(error)
    }
  };

  // // model verify login to UserGames
  verifyUserGames = async (username, password) => {
    return await db.UserGames.findOne({
      where: { username, password: md5(password) },
    });
  };
  // if user id in user biodata exist finddata
  dataUserBioExists = async (user_id) => {
    const checkUser = await db.user_biodata.findOne({
      where: { user_id: user_id },
    });
    if (checkUser) {
      return true;
    } else {
      return false;
    }
  };
  //   update when user_id exists in userbiodata table
  updateUserBio = async (firstname, lastname, fullname, address, user_id) => {
    const updateUserBio = await db.user_biodata.update(
      { firstname, lastname, fullname, address },
      { where: { user_id: user_id } }
    );
    console.log(user_id);
    return updateUserBio;
  };
  // //   insert data to user_biodata if user_id not exists
  insertUserBio = async (firstname, lastname, fullname, address, user_id) => {
    const insertUserBio = await db.user_biodata.create({
      firstname,
      lastname,
      fullname,
      address,
      user_id,
    });
    console.log(user_id, address);
    return insertUserBio;
  };
  // // get single user from database
  getSingleUserBio = async (user_id) => {
    const user = await db.user_biodata.findOne({
      where: { user_id: user_id },
    });
    return user;
  };
}

module.exports = new usersModel();
