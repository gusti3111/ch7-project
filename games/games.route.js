const express = require("express");
const gameController = require("./game.controller");
const authMiddleWare = require("../middleware/authMiddleware");
const { checkSchema } = require("express-validator");
const {
  newRecordSchema,
  getRoomGameById,
  updateGameRoom,
  newRecordGameHistories,
} = require("./game.schema");
const schemaValidation = require("../middleware/SchemaValidation");

// create a router for games
const gameRouter = express.Router();

// create new record router for game_histories
gameRouter.post(
  "/rooms",
  authMiddleWare,
  checkSchema(newRecordSchema),
  schemaValidation,
  gameController.recordNewRoom
);

// get rooms by id
gameRouter.get(
  "/rooms/:id",
  authMiddleWare,
  checkSchema(getRoomGameById),
  schemaValidation,
  gameController.getRoomsById
);

// get all rooms
gameRouter.get("/rooms", authMiddleWare, gameController.getAllRooms);

// put routing to update roomGame table and game histories when player 2 joins game
gameRouter.put(
  "/rooms/:id",
  authMiddleWare,
  checkSchema(updateGameRoom),
  schemaValidation,
  gameController.updateGameHistories
);

// get all game_histories
gameRouter.get("/", authMiddleWare, gameController.getAllGameHistories);

//create new record to table game history
gameRouter.post(
  "/signup/",
  authMiddleWare,
  checkSchema(newRecordGameHistories),
  schemaValidation,

  gameController.postGameHistories
);

module.exports = gameRouter;
