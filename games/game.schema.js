const newRecordSchema = {
  player1Choice: {
    isString: {
      errorMessage: "player1choice invalid format",
    },
    notEmpty: {
      errorMessage: "player1Choice must not be empty",
    },
  },
  roomName: {
    isString: {
      errorMessage: "roomName invalid format",
    },
    notEmpty: {
      errorMessage: "roomName must not be empty",
    },
  },
  idPlayer1: {
    isInt: {
      errorMessage: "idPlayer1 invalid format",
    },
    notEmpty: {
      errorMessage: "username must not be empty",
    },
  },
};
const getRoomGameById = {
  id: {
    in: ["params"],
    isInt: {
      errorMessage: "id invalid format",
    },
    notEmpty: {
      errorMessage: "id must not be empty",
    },
  },
};
const updateGameRoom = {
  player2Choice: {
    isString: {
      errorMessage: "player2choice invalid format",
    },
    notEmpty: {
      errorMessage: "player2Choice must not be empty",
    },
  },
  result: {
    isString: {
      errorMessage: "result invalid format",
    },
    notEmpty: {
      errorMessage: "result must not be empty",
    },
  },
  idPlayer2: {
    isInt: {
      errorMessage: "idPlayer2 invalid format",
    },
    notEmpty: {
      errorMessage: "idplayer2 must not be empty",
    },
  },
  opponent: {
    isString: {
      errorMessage: "opponent invalid format",
    },
    notEmpty: {
      errorMessage: "opponent must not be empty",
    },
  },
  user_id: {
    isInt: {
      errorMessage: "user_id invalid format",
    },
    notEmpty: {
      errorMessage: "user_id must not be empty",
    },
  },
  id: {
    in: ["params"],
    isInt: {
      errorMessage: "id invalid format",
    },
  },
};
const newRecordGameHistories = {
  opponent: {
    isString: {
      errorMessage: "opponent invalid format",
    },
    notEmpty: {
      errorMessage: "opponent must not be empty",
    },
  },
  user_id: {
    isInt: {
      errorMessage: "user_id invalid format",
    },
    notEmpty: {
      errorMessage: "user_id must not be empty",
    },
  },
  result: {
    isString: {
      errorMessage: "result invalid format",
    },
    notEmpty: {
      errorMessage: "result must not be empty",
    },
  },
};

module.exports = {
  newRecordSchema,
  getRoomGameById,
  updateGameRoom,
  newRecordGameHistories,
};
